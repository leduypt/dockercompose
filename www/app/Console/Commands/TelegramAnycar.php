<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram;

class TelegramAnycar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TelegramAnycar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Telegram::getUpdates();
        foreach ($response as $key => $value) {
            \App\Jobs\ProcessTelegramGroup::dispatch($value);
        }
    }
}
