<?php

namespace App\Helpers;

use \Illuminate\Support\Str;

class Utils
{
    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     *
     * @return string A decoded string
     */
    public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }


    public static function cleanPhone($value, $country_code = '')
    {
        $tel = preg_replace( '/[^0-9]/', '', $value );
        $tel = ltrim($tel, '0');
        if (trim($country_code) != '') {
            $len_country = strlen($country_code);
            if (substr($tel, 0, $len_country) == $country_code) {
                $tel = substr($tel, $len_country);
            }
            $tel = $country_code . $tel;
            return $tel;
        } else {
            if (substr($tel, 0, 2) == '84') {
                $tel = substr($tel, 2);
            }
            return '0' . $tel;
        }
    }


    public static function utf8_entity_decode($entity){
      $convmap = array(0x0, 0x10000, 0, 0xfffff);
      return mb_decode_numericentity($entity, $convmap, 'UTF-8');
    }
}
