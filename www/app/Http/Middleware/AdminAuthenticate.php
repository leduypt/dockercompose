<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

// extends Middleware
class AdminAuthenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if (!Auth::guard($guard)->check()) {
            return redirect()->route('backpack.auth.login');
        }
        return $next($request);
    }
}
