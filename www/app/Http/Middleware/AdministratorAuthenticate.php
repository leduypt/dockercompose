<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Closure;

// extends Middleware
class AdministratorAuthenticate
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next, $role = 'Superadmin')
    {
        // dd(backpack_user());
        if (backpack_user()->hasRole($role)) {
            return $next($request);
        }
        throw UnauthorizedException::forRoles([$role]);
    }
}
