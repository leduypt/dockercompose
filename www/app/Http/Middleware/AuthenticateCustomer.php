<?php

namespace App\Http\Middleware;

// use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

// extends Middleware
class AuthenticateCustomer
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next, $guard = 'customer')
    {
        if (!Auth::guard($guard)->check()) {
            if($guard == "admin"){
                //user was authenticated with admin guard.
                return redirect()->route('web.home.index');
            } else {
                //default guard.
                return redirect()->route('my.login_form');
            }
        }
        return $next($request);
    }
}
