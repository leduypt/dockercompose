<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ProcessTelegramGroup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $contents = [];
            if (Storage::exists('telegram_group.txt')) {
                $contents = Storage::get('telegram_group.txt');
                $contents = json_decode($contents, true);
            }

            if ($this->message->isType('message')) {
                $chat = ($this->message->getChat());
                $chat = $chat->toArray();
                $contents[$chat['id']] = $chat;
            }
            Storage::put('telegram_group.txt', json_encode($contents));
        } catch (\Exception $exception) {
            report($exception);
        }
    }

    public function failed(Exception $exception)
    {
        report($exception);
    }
}
