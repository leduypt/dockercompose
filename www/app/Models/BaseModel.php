<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class BaseModel extends Model
{
    public static function boot()
    {
        parent::boot();

        self::updated(function($model){

        });
    }

	public function __find($params=[], $new_builder = null)
    {
    	if (!isset($params['table'])) {
        	$params['table'] = $this->table;
        }
        if (!isset($params['select'])) {
            $params['select'] = '*';
        }

        $builder = self::select($params['select']);
        if ($new_builder) {
            $builder = $new_builder;
        }

        $builder = self::__array_to_builder($builder, $params);

        if (isset($params['join'])) {
            foreach ($params['join'] as $key => $value) {
                if (isset($value['where']) && is_array($value['where']) && count($value['where']) > 0) {
                    # Case ví dụ: user join post ON user.id = post.user_id AND post.type = 'car'
                    $builder = $builder->join($value['table'], function ($join) use ($value) {
                        $join_where = $value['where'];
                        $join->on($value['from'], $value['compare'], $value['to'])
                             ->where($join_where['field'], $join_where['compare'], $join_where['value']);
                    });
                } else {
                    $builder = $builder->join($value['table'], $value['from'], $value['compare'], $value['to']);
                }
            }
        }

        if (isset($params['left_join'])) {
            foreach ($params['left_join'] as $key => $value) {
                if (isset($value['where']) && is_array($value['where']) && count($value['where']) > 0) {
                    # Case ví dụ: user join post ON user.id = post.user_id AND post.type = 'car'
                    $builder = $builder->leftJoin($value['table'], function ($join) use ($value) {
                        $join_where = $value['where'];
                        $join->on($value['from'], $value['compare'], $value['to'])
                             ->where($join_where['field'], $join_where['compare'], $join_where['value']);
                    });
                } else {
                    $builder = $builder->leftJoin($value['table'], $value['from'], $value['compare'], $value['to']);
                }
            }
        }

        if (isset($params['group_by']) && $params['group_by'] != '') {
            $field = explode(',', $params['group_by']);
            foreach ($field as $key => $value) {
                $builder = $builder->groupBy($value);
            }
        }

        if (isset($params['sort_by']) && $params['sort_by'] != '') {
            $field = explode(',', $params['sort_by']);
            foreach ($field as $key => $value) {
                $tmp = explode(' ', trim($value));
                if (!preg_match('#\.#', $value)) {
                    $builder = $builder->orderBy($params['table'].'.'.$tmp[0], $tmp[1]);
                } else {
                    $builder = $builder->orderBy($tmp[0], $tmp[1]);
                }
            }
        }

        if (isset($params['page']) && intval($params['page']) > 0) {
            $old_page = (Input::get('page'));
            Input::merge(['page' => $params['page']]);
            $return = $builder->paginate($params['limit']);
            Input::merge(['page' => $old_page]);
        } else {
            if (isset($params['limit'])) {
                $builder->limit($params['limit']);
            }
            $return = $builder->get();
        }
        // echo '<pre>'; print_r($builder->toSql()); echo '</pre>';

        return $return;
    }

    public static function __array_to_builder($builder, $params)
    {
        $operators = ['>', '>=', '<', '<=', '=', '<>'];

        foreach ($params['conditions'] as $key => $value) {
            $value['compare'] = trim(strtolower($value['compare']));
            if (isset($value['field']) && !preg_match('#\.#', $value['field'])) {
                $value['field'] = $params['table'].'.'.trim($value['field']);
            }

            if ($value['compare'] == 'like') {
                $builder = $builder->where($value['field'], $value['compare'], '%'.$value['value'].'%');
            } else if ($value['compare'] == 'notnull') {
                $builder = $builder->whereNotNull($value['field']);
            } else if ($value['compare'] == 'isnull') {
                $builder = $builder->whereNull($value['field']);
            } else if ($value['compare'] == 'like_raw') {
                $builder = $builder->where($value['field'], 'like', $value['value']);
            } else if ($value['compare'] == 'in') {
                $builder = $builder->whereIn($value['field'], array_values($value['value']));
            } else if ($value['compare'] == 'notin') {
                $builder = $builder->whereNotIn($value['field'], array_values($value['value']));
            } else if (in_array($value['compare'], $operators)) {
                $builder = $builder->where($value['field'], $value['compare'], $value['value']);
            } else if ($value['compare'] == 'between') {
                $builder = $builder->whereBetween($value['field'], array_values($value['value']));
            } else if ($value['compare'] == 'notbetween') {
                $builder = $builder->whereNotBetween($value['field'], array_values($value['value']));
            } else if ($value['compare'] == 'raw') {
                $builder = $builder->whereRaw($value['value']);
            } else if (in_array($value['compare'], ['or'])) {
                $builder = $builder->orWhere(function ($query) use ($value) {
                    self::__array_to_builder($query, $value);
                });
            } else if (in_array($value['compare'], ['and'])) {
                $builder = $builder->Where(function ($query) use ($value) {
                    self::__array_to_builder($query, $value);
                });
            }
        }
        return $builder;
    }
}
