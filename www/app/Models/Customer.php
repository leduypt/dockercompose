<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Helpers\Utils;
use App\Notifications\CustomerResetPasswordNotification;

class Customer extends Authenticatable implements JWTSubject
{
    use CrudTrait;
    use Notifiable;

    const APPROVED = 1;
    const TEMPORARY = 0;
    const UNAPPROVED = -1;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'customers';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getAuthPassword()
    {
      return $this->password;
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function username_is_phone()
    {
        if ($this->username_is_email()) {
            return false;
        }
        $phone = Utils::cleanPhone($this->username);
        if (strlen($phone) >= 10) {
            return true;
        }
        return false;
    }

    public function username_is_email()
    {
        return filter_var($this->username, FILTER_VALIDATE_EMAIL) ? true : false;
    }


    public function getEmailAttribute()
    {
        if ($this->username_is_email()) {
            return $this->attributes['username'];
        } else {
            return $this->attributes['email'];
        }
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPasswordNotification($token));
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function($model){
            if ($model->username_is_email() && trim($model->email) == '') {
                $model->email = $model->username;
            }
            if ($model->username_is_phone() && trim($model->phone) == '') {
                $model->phone = $model->username;
            }
        });
    }
}
