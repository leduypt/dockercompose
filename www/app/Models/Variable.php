<?php

namespace App\Models;

use App\Helpers\Utils;

class Variable extends BaseModel
{
	protected $guarded   = [
        'id', 'created_at', 'updated_at'
    ];

    public static function get($name, $default_value = null, $cast = null)
    {
    	$var = self::where('name', $name)->first();

    	if (!$var) {
    		$var = $default_value;
    	} else {
    		$var = $var->value;
    		if ($cast !== null) {
	    		$var = $cast($var);
    		}
    	}

        return $var;
    }
}
