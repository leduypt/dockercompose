<?php
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
// use App\Traits\CustomRequests;
use View;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    // use CustomRequests;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }
}