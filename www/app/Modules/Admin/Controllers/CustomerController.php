<?php

namespace App\Modules\Admin\Controllers;

// use App\Http\Requests\CustomerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\CustomerRequest as StoreRequest;
use App\Http\Requests\CustomerRequest as UpdateRequest;
use Illuminate\Support\Facades\Hash;

/**
 * Class CustomerController
 * @package App\Modules\Admin\Controllers
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function __construct()
    {
        // $this->middleware('permission:view customers,backpack');
        parent::__construct();
    }

    public function setup()
    {
        $customer_status = config('constant.customer_status');

        $this->crud->setModel('App\Models\Customer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/customer');
        $this->crud->setEntityNameStrings('customer', 'Người dùng');
        if (!$this->request->has('order')) {
            $this->crud->orderBy('id', 'DESC');
        }
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => 'Họ tên',
            ],
            [
                'name' => 'username',
                'label' => 'Username',
            ],
            [
                'name' => 'email',
                'label' => 'Email',
            ],
            [
                'name' => 'tel',
                'label' => 'Điện thoại',
            ],
            [
                'name' => 'status',
                'label' => 'Trạng thái',
                'type' => 'closure',
                'options' => config('constant.customer_status'),
                'priority' => 1,
                'function' => function($entry) {
                    $status = isset(config('constant.customer_status')[$entry->status]) ? config('constant.customer_status')[$entry->status] : '';
                    $class = 'label-default';
                    switch ($entry->status) {
                        case \App\Models\Customer::APPROVED:
                            $class = 'label-success';
                            break;
                        case \App\Models\Customer::UNAPPROVED:
                            $class = 'label-danger';
                            break;
                    }
                    return '<span class="label '.$class.'">'.$status.'</span>';
                }
            ],
        ]);

        $show_column_action = 2;
        if (!backpack_user()->hasRole('Superadmin')) {
            $this->crud->denyAccess('create');
        }
        if (!backpack_user()->hasRole('Superadmin')) {
            $this->crud->denyAccess('update');
            $show_column_action--;
        }
        if (!backpack_user()->hasRole('Superadmin')) {
            $this->crud->denyAccess('delete');
            $show_column_action--;
        }
        if (!$show_column_action) {
            $this->crud->removeAllButtonsFromStack('line');
        }
    }

    protected function setupCreateOperation()
    {
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // $this->crud->setValidation(CustomerRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();
        $this->crud->addFilter(
            [
                'name' => 'status',
                'type' => 'dropdown',
                'label'=> 'Trạng thái'
            ],
            config('constant.customer_status'),
            function($value) {
                $this->crud->addClause('where', 'status', $value);
            }
        );

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Họ tên'
            ],
            [
                'name' => 'username',
                'type' => 'text',
                'label' => 'Username'
            ],
            [
                'name'  => 'password',
                'label' => 'Password',
                'type'  => 'password',
            ],
            [
                'name'  => 'password_confirmation',
                'label' => 'Password Confirmation',
                'type'  => 'password',
            ],
            [
                'name' => 'phone',
                'type' => 'text',
                'label' => 'Điện thoại'
            ],
            [
                'name' => 'email',
                'type' => 'email',
                'label' => 'Email'
            ],
            [
                'name' => 'address',
                'type' => 'text',
                'label' => 'Địa chỉ'
            ],
            [
                'name' => 'status',
                'type' => 'select_from_array',
                'label' => 'Trạng thái',
                'options' => config('constant.customer_status'),
                'default' => \App\Models\Customer::APPROVED
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request);
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitStore();
    }

    /**
     * Update the specified resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request); 
        $this->crud->unsetValidation(); // validation has already been run  

        return $this->traitUpdate();
    }
    
    /**
     * Handle password input fields.
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }
        return $request;
    }
}
