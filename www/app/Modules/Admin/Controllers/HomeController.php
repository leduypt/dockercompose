<?php
namespace App\Modules\Admin\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // echo __FILE__ . ': ' . __LINE__; die;
        return view('Admin::home.index');
    }
}
