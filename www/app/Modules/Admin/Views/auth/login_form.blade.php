@extends('Admin::layouts.blank')

@section('content')
    <!-- <header id="header">
        <div id="logo-group">
            <span id="logo"><b>F1 MY</b></span>
        </div>
    </header> -->

    <!-- MAIN CONTENT -->
    <div id="content" class="container">
        <h1 class="text-center padding-bottom-10">Đăng nhập hệ thống</h1>
        <div class="text-center">
            @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>
        <div class="text-center">
            <a href="{{ config('app.sso_url') . route('admin.login.sso.callback') }}" class="btn btn-labeled btn-danger">
                <span class="btn-label"><img src="{{ asset('assets/img/logo-white.png') }}" height="20"></span>
                Đăng nhập bằng email Anycar
            </a>
        </div>
    </div>
@endsection


@section('header_css')
<style type="text/css">
#header {
    display: block;
    height: 49px;
    margin: 0;
    padding: 0 13px 0 0;
    background-color: #f3f3f3;
    background-image: -moz-linear-gradient(top,#f3f3f3,#e2e2e2);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#f3f3f3),to(#e2e2e2));
    background-image: -webkit-linear-gradient(top,#f3f3f3,#e2e2e2);
    background-image: -o-linear-gradient(top,#f3f3f3,#e2e2e2);
    background-image: linear-gradient(to bottom,#f3f3f3,#e2e2e2);
    background-repeat: repeat-x;
    position: relative;
    z-index: 905;
}
#header>:first-child, aside {
    width: 220px;
}
#header>div {
    display: inline-block;
    vertical-align: middle;
    height: 49px;
    float: left;
}
#logo-group>span {
    display: inline-block;
    height: 39px;
    float: left;
}
#logo {
    display: inline-block;
    width: 175px;
    margin-top: 13px;
    margin-left: 9px;
}
.padding-bottom-10 {
    padding-bottom: 10px!important;
}
#content {
    padding: 10px 14px;
    position: relative;
}
h1 {
    letter-spacing: -1px;
    font-size: 24px;
    margin: 10px 0;
}
.btn-label {
    position: relative;
    left: -12px;
    display: inline-block;
    padding: 6px 12px;
    background: rgba(0,0,0,.15);
    border-radius: 3px 0 0 3px;
}
.btn, a:link, button {
    -webkit-tap-highlight-color: rgba(169,3,41,.5);
}
.btn-labeled {
    padding-top: 0;
    padding-bottom: 0;
}
</style>
@endsection
