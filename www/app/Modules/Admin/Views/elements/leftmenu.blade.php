@php
$user = Auth::guard('admin')->user();
$process_url = 'admin.home';
// if ($user->hasRole('f1.buy.coordinator@anycar.vn')) {
//     $process_url = 'admin.appointment.calendar';
// } else if ($user->hasRole('f1.buy.receptionist@anycar.vn')) {
//     $process_url = 'admin.appointment.index';
// } else if ($user->hasRole('f1.buy.appraiser@anycar.vn')) {
//     $process_url = 'admin.carcheck.index';
// } else if ($user->hasRole('f1.buy.administrator@anycar.vn')) {
//     $process_url = 'admin.contract.index';
// } else if ($user->hasRole('f1.buy.leader@anycar.vn')) {
//     $process_url = 'admin.appointment.index';
// }

$menu = [
    [
        'title' => 'Dashboard',
        'link' => route('admin.home'),
        'icon' => 'fa-dashboard',
        'permission' => ''
    ],
    [
        'title' => 'Danh sách vị trí bản đồ',
        'link' => backpack_url('map'),
        'icon' => 'fa-list-alt',
        'permission' => 'map-list'
    ],
    // [
    //     'title' => 'Quản lý thiết bị',
    //     'link' => backpack_url('tablet'),
    //     'icon' => 'fa-mobile',
    //     'permission' => 'device-list'
    // ],
    // [
    //     'title' => 'Quản trị viên',
    //     'link' => route('admin.user.list'),
    //     'icon' => 'fa-user',
    //     'permission' => 'user-list'
    // ],
    // [
    //     'title' => 'Nhóm quản trị',
    //     'link' => route('admin.role.list'),
    //     'icon' => 'fa-users',
    //     'permission' => 'role-list'
    // ],
];
@endphp
<ul class="sidebar-menu" data-widget="tree">
{{-- <li class="header">MAIN NAVIGATION</li> --}}
@foreach ($menu as $item)
    @if ($item['permission'] != '' && !$user->can($item['permission']))
        @continue
    @endif
    <li>
        <a href="{{ $item['link'] }}" title="{{ $item['title'] }}">
            <i class="fa fa-lg fa-fw {{ $item['icon'] }}"></i>
            <span> {{ $item['title'] }}</span>
            {{-- <span class="menu-item['arent']">{{ $item['title'] }}</span> --}}
        </a>
    </li>
@endforeach
</ul>