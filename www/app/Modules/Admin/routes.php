<?php
Route::group([
    'middleware' => ['web',config('backpack.base.middleware_key', 'admin')],
    'namespace'  => '\App\Modules\Admin\Controllers',
], function () { // custom admin routes
    Route::get('/logout', 'AuthController@logout')->name('admin.logout');
    Route::get('/', 'HomeController@index')->name('admin.home');
    Route::crud('customer', 'CustomerController');

    // Backpack\NewsCRUD
    Route::crud('article', 'ArticleController');
    Route::crud('category', 'CategoryController');
    Route::crud('tag', 'TagController');

}); // this should be the absolute last line of this file

Route::group(
[
    'namespace'  => 'Backpack\CRUD\app\Http\Controllers',
    'middleware' => 'web'
],
function () {
    // if not otherwise configured, setup the auth routes
    // if (config('backpack.base.setup_auth_routes'))
    {
        // Authentication Routes...
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('backpack.auth.login');
        Route::post('login', 'Auth\LoginController@login');
        Route::get('logout', 'Auth\LoginController@logout')->name('backpack.auth.logout');
        Route::post('logout', 'Auth\LoginController@logout');

        // Registration Routes...
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('backpack.auth.register');
        Route::post('register', 'Auth\RegisterController@register');

        // Password Reset Routes...
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('backpack.auth.password.reset');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('backpack.auth.password.reset.token');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('backpack.auth.password.email');
    }

    // if not otherwise configured, setup the dashboard routes
    if (config('backpack.base.setup_dashboard_routes')) {
        Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
        Route::get('/', 'AdminController@redirect')->name('backpack');
    }

    // if not otherwise configured, setup the "my account" routes
    if (config('backpack.base.setup_my_account_routes')) {
        Route::get('edit-account-info', 'MyAccountController@getAccountInfoForm')->name('backpack.account.info');
        Route::post('edit-account-info', 'MyAccountController@postAccountInfoForm');
        Route::post('change-password', 'MyAccountController@postChangePasswordForm')->name('backpack.account.password');
    }
});


