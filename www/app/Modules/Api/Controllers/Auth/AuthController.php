<?php
namespace App\Modules\Api\Controllers\Auth;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Api\Resources\UserResource;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // Config Module Auth JWT Guard
        config()->set( 'auth.defaults.guard', 'api' );

        // Validate Data
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:customers,email|email',
            'password' => 'required'
        ]);

        $user = Customer::create([
            'name' => $request->name,
            'username' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => Customer::TEMPORARY
        ]);
        
        // Auto Login => Return User
        if(!$token = JWTAuth::attempt($request->only(['email', 'password'])))
        {
            return abort(401);
        }
        return (new UserResource($user))
        ->additional([
            'status' => 'success',
            'data' => [ 
                'token' => $token
            ]
        ]);
    }

    public function login(Request $request)
    {
        // Config Module Auth JWT Guard
        config()->set( 'auth.defaults.guard', 'api' );

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = request(['email', 'password']);

        if(!$token = auth('api')->attempt($credentials)){
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'There is something wrong! We could not verify details'
            ]], 422);
        }

        return (new UserResource($request->user()))
        ->additional([
            'status' => 'success',
            'data' => [
                'message' => 'Login successfuly!',
                'token' => $token
            ]
        ]);
    }

    public function me()
    {
      
        try {
            $dataUser = auth('api')->user();
            return [
                'data' => $dataUser
            ];
        } catch (JWTException $e) {
            return response()->json('Failed to get data, please try again.', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);
        
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json('You have successfully logged out.', Response::HTTP_OK);
        } catch (JWTException $e) {
            return response()->json('Failed to logout, please try again.', Response::HTTP_BAD_REQUEST);
        }
    }

    public function refresh()
    {
        try {
            $token_refresh = auth('api')->refresh();
            return [
                'token_refresh' => $token_refresh
            ];
        } catch (JWTException $e) {
            return response()->json('Failed to refresh token, please try again.', Response::HTTP_BAD_REQUEST);
        }
    }
}