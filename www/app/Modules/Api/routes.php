<?php
Route::group(
	[
		// 'domain' => env('APP_WEBSITE_DOMAIN'),
		'middleware' => 'api',
		'namespace' => '\App\Modules\Api\Controllers'
	],
    function() {
		Route::post('register', 'Auth\AuthController@register')->name('register');
		Route::post('login', 'Auth\AuthController@login')->name('login');
    }
);

Route::group([
    'middleware' => ['jwt', 'jwt.auth'],
	'namespace' => '\App\Modules\Api\Controllers'
], function() {
    Route::get('me', 'Auth\AuthController@me')->name('getUser');
});

Route::middleware('jwt.refresh')->get('/token/refresh', '\App\Modules\Api\Controllers\Auth\AuthController@refresh')->name('refresh_token');

