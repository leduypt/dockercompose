<?php
namespace App\Modules;
use File;
use Illuminate\Http\Request;

class ServiceProvider extends  \Illuminate\Support\ServiceProvider{
    public function boot() {
        if (!isset($_SERVER['SERVER_NAME'])) {
            return true;
        }

        $current_domain = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        $urlConfig = config('app.url_config');
        
        foreach ($urlConfig as $module => $domain) {
            if (!preg_match('#\:#', $domain)) {
                $domain .= ':80';
            }

            if ($domain == $current_domain) {
                include __DIR__.'/'.$module.'/routes.php';
                if(is_dir(__DIR__.'/'.$module.'/Views')) {
                    $this->loadViewsFrom(__DIR__.'/'.$module.'/Views', $module);
                }
                break;
            }
        }
    }
    public function register(){}
}
