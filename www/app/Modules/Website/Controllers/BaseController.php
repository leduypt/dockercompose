<?php
namespace App\Modules\Website\Controllers;

use App\Http\Controllers\Controller;
// use App\Traits\CustomRequests;
use View;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    // use CustomRequests;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     if (!Auth::check()) {
        //         $request->session()->put('previous_url', url()->current());
        //         return redirect(route('acc.login'));
        //     }

        //     $this->user = Auth::user();
        //     View::share('user', $this->user);

        //     return $next($request);
        // });
    }
}
