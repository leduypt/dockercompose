<?php
Route::group(
	[
		// 'domain' => env('APP_WEBSITE_DOMAIN'),
		'middleware' => 'web',
		'namespace' => '\App\Modules\Website\Controllers'
	],
    function() {
	    Route::get('/', 'HomeController@index')->name('web.home.index');
    }
);
