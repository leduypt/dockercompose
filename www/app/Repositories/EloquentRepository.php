<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class EloquentRepository implements RepositoryInterface
{
	protected $model;

	public function __construct()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        $model = app()->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function all($columns = array('*'))
    {
    	return $this->model->all($columns);
    }

    public function list(array $params)
    {
    	return $this->model->__find($params);
    }

    public function find($id)
    {
    	return $this->model->find($id);
    }
}
