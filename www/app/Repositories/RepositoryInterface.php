<?php

namespace App\Repositories;

interface RepositoryInterface
{
    public function all($columns = array('*'));

    public function list(array $params);

    public function find($id);
}
