<?php

namespace App\Repositories;

use App\Repositories\Contracts\EloquentRepository;
use App\Models\Variable;

class VariableRepository extends EloquentRepository
{
	public function model()
	{
		return Variable::class;
	}
}