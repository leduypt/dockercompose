#!/bin/sh

php artisan clear-compiled
php artisan optimize:clear
php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan view:clear
