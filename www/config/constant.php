<?php

return [
    'customer_status' => [
    	App\Models\Customer::APPROVED => 'Kích hoạt',
    	App\Models\Customer::TEMPORARY => 'Chờ duyệt',
    	App\Models\Customer::UNAPPROVED => 'Xóa'
    ],
];
