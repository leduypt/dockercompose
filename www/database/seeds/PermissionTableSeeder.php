<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('model_has_permissions')->delete();
        DB::table('model_has_roles')->delete();
        
        $permissions = [
           'map-list'
        ];

        foreach ($permissions as $permission_name) {
            $permission = Permission::create(['guard_name' => 'web', 'name' => $permission_name]);
        }

        $role = Role::create(['guard_name' => 'web', 'name' => 'f1.bigboss.superadmin@anycar.vn']);
        $role->syncPermissions($permissions);
    }
}